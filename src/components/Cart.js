import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { addQuantity, removeItem, subtractQuantity } from "./actions/cartActions";

export const Cart = (props) => {
  const { items, removeItem, addQuantity, subtractQuantity } = props;
  // to remove the item completely
  const handleRemove = (id) => {
    removeItem(id);
  };
  // to add the quantity
  const handleAddQuantity = (id) => {
    addQuantity(id);
  };
  // to substruct from the quantity
  const handleSubtractQuantity = (id) => {
    subtractQuantity(id);
  };

  const totalPrice = items.map(item => item.price * item.quantity).reduce((a, b) => a + b, 0);

  const addedItems = items.length
    ? (
      <div className="collection">
        {items.map((item) => (
          <div className="collection-item avatar" key={item.id}>
              <div className="item-img">
                <img src={item.img} alt={item.img} className=""/>
              </div>

              <div className="item-desc">
                <span className="title">{item.title}</span>
                <p>{item.desc}</p>
                <p>
                  <b>
                    Price:
                    {item.price}
                    $
                  </b>
                </p>
                <p>
                  <b>
                    Quantity:
                    {item.quantity}
                  </b>
                </p>
                <div className="add-remove">
                  <Link to="/cart">
                    <i
                      className="material-icons"
                      onClick={() => {
                        handleAddQuantity(item.id);
                      }}
                    >
                      arrow_drop_up
                    </i>
                  </Link>
                  <Link to="/cart">
                    <i
                      className="material-icons"
                      onClick={() => {
                        handleSubtractQuantity(item.id);
                      }}
                    >
                      arrow_drop_down
                    </i>
                  </Link>
                </div>
                <button
                  className="waves-effect waves-light btn pink remove"
                  onClick={() => {
                    handleRemove(item.id);
                  }}
                >
                  Remove
                </button>
              </div>

            </div>
        ))}
      </div>
    ) : (
      <p>Nothing.</p>
    );

  return (
    <div className="container">
      <div className="cart">
        <div className="cart-header">
          <h5>You have ordered:</h5>
          {totalPrice > 0 && (
            <span>Total: {totalPrice}$</span>
          )}
        </div>
        {addedItems}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  items: state.addedItems
  // addedItems: state.addedItems
});
const mapDispatchToProps = (dispatch) => ({
  removeItem: (id) => { dispatch(removeItem(id)); },
  addQuantity: (id) => { dispatch(addQuantity(id)); },
  subtractQuantity: (id) => { dispatch(subtractQuantity(id)); }
});
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
